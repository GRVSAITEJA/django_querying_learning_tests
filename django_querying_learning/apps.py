from django.apps import AppConfig


class DjangoQueryingLearningConfig(AppConfig):
    name = 'django_querying_learning'
